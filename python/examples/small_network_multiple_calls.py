import efmtool
import numpy as np

if __name__ == "__main__":
    # Network structure:
    # => A, A <=> B, B <=> C, A => C, C <=>
    S = np.array(
        [
            [1, -1, 0, -1, 0],
            [0, 1, -1, 0, 0],
            [0, 0, 1, 1, -1],
        ]
    )
    rev = [0, 1, 1, 0, 1]
    metabolites = ["A", "B", "C"]
    reactions = ["r1", "r2", "r3", "r4", "r5"]
    efms1 = efmtool.calculate_efms(S, rev, reactions, metabolites)
    efms2 = efmtool.calculate_efms(S, rev, reactions, metabolites)

    assert efms1.shape == efms2.shape
    print(f"Found {efms1.shape[1]} EFMs.")
