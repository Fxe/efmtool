/* ==========================================================================
 * copyright (c) javasoft.ch / marco terzer
 * ==========================================================================
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * --------------------------------------------------------------------------
 *
 * contact or feedback to: info@javasoft.ch
 * 
 */
package ch.javasoft.jbase;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ch.javasoft.jbase.concurrent.StatefulTable;
import ch.javasoft.jbase.concurrent.UnsupportedReadCopyException;



/**
 * The <code>VariableWidthTablePaired</code> stores entities of unknown, 
 * variable size. A pair of tables is used to store the rows. If necessary, a 
 * pointer in the first table points to the entry in another table of sufficient
 * row width.
 * <p>
 * Multiple files are used to store the data, each file controlled by a
 * {@link FixedWidthTable}. The primary table contains raw data plus two 
 * additional indices pointing to an entry of the secondary table (one index for
 * the table, one for the row in the table). Multiple secondary tables of 
 * different sizes exist, each storing raw data plus an index bach to the row
 * of the primary table (needed for deletions). The widths of the secondary 
 * tables are powers of two. The smallest possible secondary table is used
 * and created if not yet existing.
 */
public class VariableWidthTable<E> implements StatefulTable<E> {
    
	private final Lock						openTableLock;
    private final File                  	folder;
    private final String                	fileName;
    private final EntityMarshaller<E>   	entityMarshaller;    
    private FixedWidthTable<FixedTableRow> 	primaryTable;
    
    private final NavigableMap<File, FixedWidthTable<FixedTableRow>> secondaryTables;

    private final ByteArray byteBuffer;    		
    
    /**
     * Constructor, only for subclassing, use the static 
     * {@link #create(File, String, int, EntityMarshaller) create} and 
     * {@link #open(File, String, EntityMarshaller) open} methods to create 
     * instances.
     * 
     * @param folder            The folder containing the table files
     * @param fileName          The file name of the tables. Indexing is inserted
     *                          in front of the file ending if needed. 
     * @param entityMarshaller  The marshaller transforming entities (rows) into bytes  
     */
    protected VariableWidthTable(File folder, String fileName, EntityMarshaller<E> entityMarshaller) {
    	this.openTableLock		= new ReentrantLock();
        this.folder             = folder;
        this.fileName           = fileName;
        this.entityMarshaller	= entityMarshaller;
        this.secondaryTables    = new TreeMap<File, FixedWidthTable<FixedTableRow>>();
        this.byteBuffer			= new ByteArray();
    }

    /**
     * Opens existing variable table files.
     * 
     * @param folder            The folder containing the table files
     * @param fileName          The file name of the tables. Indexing is inserted
     *                          in front of the file ending if needed. 
     * @param entityMarshaller  The marshaller transforming entities (rows) into bytes  
     */
    public static <En> VariableWidthTable open(File folder, String fileName, EntityMarshaller<En> entityMarshaller) throws IOException {
        final VariableWidthTable<En> tbl = new VariableWidthTable<En>(folder, fileName, entityMarshaller);
        tbl.primaryTable = tbl.openPrimaryTable();
        return tbl;
    }
    /**
     * Creates new variable table files, erasing existing files if existent. 
     * <p>
     * The width of the primary table controls the frequency of single/double table 
     * accesses versus gaps in the primary table. If the primary table is wide, the 
     * majority of accesses only concern the primary table, but many rows might only 
     * use a small portion of the fixed row width. If the primary table is small, less 
     * disk space is needed, but secondary table accesses are more likely.
     * 
     * @param folder            The folder containing the table files
     * @param fileName          The file name of the tables. Indexing is inserted
     *                          in front of the file ending if needed. 
     * @param firstTableByteWidth   The byte width of the primary table, without
     *                              index widths for indices to secondary tables
     * @param entityMarshaller  The marshaller transforming entities (rows) into bytes  
     */
    public static <En> VariableWidthTable<En> create(File folder, String fileName, int firstTableByteWidth, EntityMarshaller<En> entityMarshaller) throws IOException {
        final VariableWidthTable<En> tbl = new VariableWidthTable<En>(folder, fileName, entityMarshaller);
        eraseTableFiles(folder, fileName);
        tbl.primaryTable = tbl.createPrimaryTable(firstTableByteWidth);
        return tbl;
    }
    
    private static void eraseTableFiles(File folder, String fileName) {
    	final String prefix 	= getFileNamePrefix(fileName);
    	final String postfix	= getFileNamePostfix(fileName);
        final File[] files = folder.listFiles(new FilenameFilter() {
        	public boolean accept(File dir, String name) {
        		return name.startsWith(prefix) && name.endsWith(postfix);
        	}
        });
        for (final File file : files) {
        	file.delete();
        }        
    }
    
    private FixedWidthTable<FixedTableRow> openPrimaryTable() throws IOException {
    	return openTableFile(0, 0, false);
    }
    private FixedWidthTable<FixedTableRow> createPrimaryTable(int rawByteWidth) throws IOException {
        final int byteWidth = getTableByteWidth(0, rawByteWidth);
    	return openTableFile(0, byteWidth, true);
    }
    private FixedWidthTable<FixedTableRow> openSecondaryTable(int rawByteWidth, boolean createIfNeeded) throws IOException {
        final int byteWidth = getTableByteWidth(1, rawByteWidth);
        final File file = getTableFile(1, byteWidth);
        FixedWidthTable<FixedTableRow> tbl = secondaryTables.get(file);
        if (tbl == null) {
        	tbl = openTableFile(1, byteWidth, createIfNeeded);
        	secondaryTables.put(file, tbl);
        }
        return tbl;
    }
    protected FixedWidthTable<FixedTableRow> openTableFile(int tableIndex, int byteWidth, boolean createIfNeeded) throws IOException {
        final File file = getTableFile(tableIndex, byteWidth);
        if (file.exists()) {
        	openTableLock.lock();
        	try {
	            final RandomAccessPersister rap = new RandomAccessFilePersistor(file);
	            final int width = FixedWidthTable.readByteWidth(rap);
	            final FixedTableRow row = FixedTableRow.getByByteArrayLength(width, tableIndex == 0 ? 2 : 1);
	            return FixedWidthTable.open(rap, row);
        	}
        	finally {
        		openTableLock.unlock();
        	}
        }
        else {
        	if (createIfNeeded) {
                final RandomAccessPersister rap = new RandomAccessFilePersistor(file);
                final int width = getTableByteWidth(tableIndex, byteWidth);
                final FixedTableRow row = FixedTableRow.getByByteArrayLength(width, tableIndex == 0 ? 2 : 1);
                return FixedWidthTable.create(rap, row);        		
        	}
        	else {
        		throw new IOException("no such table: " + file.getAbsolutePath());
        	}
        }
    }
    
    protected static String getFileNamePrefix(String fileName) {
        final int lastDot = fileName.lastIndexOf('.');
        return lastDot < 0 ? fileName : fileName.substring(0, lastDot);
    }
    protected static String getFileNamePostfix(String fileName) {
        final int lastDot = fileName.lastIndexOf('.');
        return lastDot < 0 ? "" : fileName.substring(lastDot);
    }
    protected File getTableFile(int tableIndex, int byteWidth) {
    	if (tableIndex == 0) {
    		return new File(folder, fileName); 
    	}
        final int lastDot = fileName.lastIndexOf('.');
        if (lastDot < 0) {
            return new File(folder, fileName + byteWidth);
        }
        return new File(folder, fileName.substring(0, lastDot) + byteWidth + fileName.substring(lastDot));
    }
    
    /**
     *
     */
    protected int getTableByteWidth(int tableIndex, int rawByteWidth) throws IOException {
    	int width;
    	if (tableIndex == 0) {
    		// we want next larger size dividable by 4
    		width = 4 + 4 * (rawByteWidth / 4); 
    	}
    	else {
    		// we want powers of two
    		width = 4;
        	while (width < rawByteWidth && width > 0) {
                width <<= 1;
            }
    	}
     	if (width < rawByteWidth) {
     		//overflow
     		throw new IOException("table width overflow: " + rawByteWidth);
     	}
        return width;
    }
    
    public void addEntity(E entity) throws IOException {
        if (primaryTable == null) throw new IOException("table already closed");

        byteBuffer.reset();
//        final DataOutputStream das = new DataOutputStream(byteBuffer);
        final DataOutputStream das = byteBuffer.getDataOutputStream();
        entityMarshaller.writeTo(entity, das);
        das.flush();
        
        // write to primary table
        FixedTableRow row0 = (FixedTableRow)primaryTable.getEntityMarshaller();
        row0.getBytesFrom(byteBuffer);
        
        final int remainingBytes = byteBuffer.getLength();
        if (remainingBytes != 0) {
        	final FixedWidthTable<FixedTableRow> secondaryTable = openSecondaryTable(
        			remainingBytes, true /*createIfNeeded*/);
            FixedTableRow row1 = (FixedTableRow)secondaryTable.getEntityMarshaller();
            row1.getBytesFrom(byteBuffer);
            row1.setInt(0, primaryTable.size());
            if (!byteBuffer.isEmpty()) {
            	throw new IOException("internal error, buffer not empty after flushing to secondary table"); 
            }
            final int index1 = secondaryTable.size();
            secondaryTable.addEntity(row1);
            row0.setInt(0, row1.getByteArrayLength());
            row0.setInt(1, index1);
//            if (index1 < 10 && secondaryTable.getByteWidth() == 4) {
//                System.out.println("w[" + primaryTable.size() + 
//                        "/" + secondaryTable.size() + "] = " + row0.toString() + row1);
//            }
        }
        else {
        	row0.setInt(0, 0);
        	row0.setInt(1, 0);
        }
        primaryTable.addEntity(row0);
    }

    public void close() throws IOException {    	
        for (final FixedWidthTable<FixedTableRow> tbl : secondaryTables.values()) {
        	tbl.close();
        }
        primaryTable.close();
        secondaryTables.clear();
        primaryTable = null;
    }

    public void flush() throws IOException {
        if (primaryTable == null) throw new IOException("table already closed");
        for (final FixedWidthTable<FixedTableRow> tbl : secondaryTables.values()) {
        	tbl.flush();
        }
        primaryTable.flush();
    }
    /**
     * Calls {@link #close()}
     */
    @Override
    protected void finalize() throws Throwable {
    	close();
    }
    
    /**
     * Returns true if the primary table supports read copies, i.e. always true.
     * @return always true
     */
    public boolean isReadCopySupported() {
    	return primaryTable.isReadCopySupported();
    }

    public VariableWidthTable<E> createReadCopy() throws IOException, UnsupportedReadCopyException {
    	final VariableWidthTable<E> tbl = new VariableWidthTable<E>(folder, fileName, entityMarshaller) {
			@Override
		    protected FixedWidthTable<FixedTableRow> openTableFile(int tableIndex, int byteWidth, boolean createIfNeeded) throws IOException {
				if (createIfNeeded) {
					throw new IOException("internal error: read only table");
				}
				final FixedWidthTable<FixedTableRow> main = VariableWidthTable.this.openTableFile(tableIndex, byteWidth, createIfNeeded);
				return main.createReadCopy();
			}
			@Override
			public void addEntity(E entity) throws IOException {
				throw new IOException("unmodifyable table");    							
			};
			@Override
			public void setEntity(int index, E entity) throws IOException {
				throw new IOException("unmodifyable table");    							
			};
			@Override
			public void removeEntity(int index) throws IOException {
				throw new IOException("unmodifyable table");    							
			}
			@Override
			public void removeAll() throws IOException {
				throw new IOException("unmodifyable table");    							
			}
		};
		tbl.openPrimaryTable();
		return tbl;
    }
    
    public E getEntity(int index) throws IOException {
        if (primaryTable == null) throw new IOException("table already closed");
        
        byteBuffer.reset();
        
        final FixedTableRow row0 = primaryTable.getEntity(index);
        row0.putBytesTo(byteBuffer);
        
        final int tbl1 = row0.getInt(0);
        final int ind1 = row0.getInt(1);
        if (tbl1 != 0) {
        	final FixedWidthTable<FixedTableRow> secondaryTable = openSecondaryTable(
        			tbl1, false /*createIfNeeded*/);
            final FixedTableRow row1 = secondaryTable.getEntity(ind1);
            row1.putBytesTo(byteBuffer);
//            if (ind1 < 10) {
//                System.out.println("r[" + primaryTable.size() + 
//                        "/" + secondaryTable.size() + "] = " + row0.toString() + row1);
//            }
        }
        
        return entityMarshaller.readFrom(byteBuffer.getDataInputStream());
    }

    public void removeEntity(int index) throws IOException {
        if (primaryTable == null) throw new IOException("table already closed");
        
        //just for consistent naming throughout this method
        final FixedWidthTable<FixedTableRow> fwt0 = primaryTable;
        final int ind0 = index;
        
        FixedTableRow row0 = (FixedTableRow)fwt0.getEntityMarshaller();
        row0.setIndexOnlyMode(true);
        row0 = fwt0.getEntity(ind0);
        row0.setIndexOnlyMode(false);
        
        final int tbl1 = row0.getInt(0);
        final int ind1 = row0.getInt(1);
        
        //NOTE: the following actions are performed
        //1a) delete entry in primary table, which moves the last entry to the
        //    gap position
        //1b) if the moved entry has a corresponding entry in the secondary 
        //    table, its back pointer has to be updated
        //2a) if the deleted entry had an entry in the secondary table, also
        //    delete it, again moving the last table entry to the gap position
        //2b) the primary entry corresponding to the moved entry has to be
        //    updated, too
        //2c) delete secondary table if it is empty
        
        //1a) delete primary
        fwt0.removeEntity(ind0);
        final int last0 = primaryTable.size();
        
        //1b) update secondary of moved
        if (ind0 != last0) {
            row0.setIndexOnlyMode(true);
            row0 = fwt0.getEntity(ind0);
            row0.setIndexOnlyMode(false);
            final int tbl2 = row0.getInt(0);
            final int ind2 = row0.getInt(1);
            
            if (tbl2 != 0) {
                final FixedWidthTable<FixedTableRow> fwt2 = openSecondaryTable(
            			tbl2, false /*createIfNeeded*/);
                final FixedTableRow row2 = (FixedTableRow)fwt2.getEntityMarshaller();
                row2.setIndexOnlyMode(true);
                row2.setInt(0, ind0);
                fwt2.setEntity(ind2, row2);
                row2.setIndexOnlyMode(false);
            }
        }
        
        //2a) delete secondary
        if (tbl1 != 0) {
            final FixedWidthTable<FixedTableRow> fwt1 = openSecondaryTable(
        			tbl1, false /*createIfNeeded*/);
            fwt1.removeEntity(ind1);
            final int last1 = fwt1.size();
            
            //2b) update primary of moved
            if (ind1 != last1) {
                FixedTableRow row1 = (FixedTableRow)fwt1.getEntityMarshaller();
                row1.setIndexOnlyMode(true);
                row1 = fwt1.getEntity(ind1);
                row1.setIndexOnlyMode(false);
                final int ind0b = row1.getInt(0);

                row0.setIndexOnlyMode(true);
                row0.setInt(0, tbl1);
                row0.setInt(1, ind1);
                fwt0.setEntity(ind0b, row0);
                row0.setIndexOnlyMode(false);
            }
            
            //2c) delete if empt
            if (fwt1.size() == 0) {
            	fwt1.close();
            	final File file = getTableFile(1, tbl1);
            	file.delete();
            	if (secondaryTables.remove(file) != fwt1) {
            		throw new IOException("internal error, should have deleted file of current table: " + file.getAbsolutePath());
            	}
            }
        }
        
    }
    
    public void removeAll() throws IOException {
    	final int primaryByteWidth = primaryTable.getByteWidth() - 8;
    	close();
    	eraseTableFiles(folder, fileName);
    	primaryTable = createPrimaryTable(primaryByteWidth);
    }

    public void setEntity(int index, E entity) throws IOException {
        if (primaryTable == null) throw new IOException("table already closed");

        addEntity(entity);
        removeEntity(index);
    }

    public int size() throws IOException {
        if (primaryTable == null) throw new IOException("table already closed");
        return primaryTable.size();
    }

}
